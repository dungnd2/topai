import { createStackNavigator, createAppContainer } from 'react-navigation'
import LaunchScreen from '../Containers/LaunchScreen'
import CameraView from '../Containers/CameraView'
import AnswerScreen from '../Containers/AnswerScreen'
import ImageCropper from '../Containers/ImageCropper'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  LaunchScreen: { screen: LaunchScreen },
  CameraScreen: { screen: CameraView },
  AnswerScreen: { screen: AnswerScreen },
  ImageCropper: { screen: ImageCropper }
}, {
  // Default config for all screens
  visible: true,
  initialRouteName: 'LaunchScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default createAppContainer(PrimaryNav)
