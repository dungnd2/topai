import { call, put, select ,take} from 'redux-saga/effects'
import { NavigationActions } from 'react-navigation';
import ImageActions, { ImageSelectors } from '../Redux/ImageRedux'
import { Alert } from 'react-native'
const selectImage = (state) => state.image

export function* uploadImage(api) {
  const temp =  yield select (selectImage)
  let data = {img:temp.imageBase64}
  var filename = temp.imageUrl.substring(temp.imageUrl.lastIndexOf('/')+1);
  const response = yield call(api.uploadImage, data)
  if (response.ok) {
    let temp = response.data.result.toString()
    console.log('ok '+JSON.stringify(temp))
    yield put ({type: 'UPLOAD_FINISH'})
    yield put(NavigationActions.navigate({ routeName: 'AnswerScreen',params:{data:response.data.result}}));
  } else {
    console.log("FAIL"+JSON.stringify(response))
    yield put ({type: 'UPLOAD_FINISH'})
    Alert.alert(response.problem)

  }
}
