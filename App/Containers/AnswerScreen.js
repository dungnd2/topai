import React, { Component } from 'react'
import { ScrollView, Text, Image, View, TouchableOpacity, ActivityIndicator } from 'react-native'
import { Images, Metrics, Colors } from '../Themes'
import { RNCamera } from 'react-native-camera';
import Modal from "react-native-modal";
import styles from './Styles/LaunchScreenStyles'
import { Icon } from 'react-native-elements'
import { connect } from 'react-redux';
import { ImageSelectors } from '../Redux/ImageRedux'
import MathJax from 'react-native-mathjax'
import _ from 'lodash';

class AnswerScreen extends Component {
  static navigationOptions = {
    title: 'Answer',
    headerStyle: {
      backgroundColor: Colors.mainColor,
    },
    headerTintColor: "#2D1E36",
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };
  constructor(props) {
    super(props)
    this.state = {
      isVisible: false
    }
  }

  componentDidMount() {
    console.log()
  }

  render() {
    let data = this.props.navigation.getParam('data')
    data = _.trimStart(data, '![]');
    let indexRegex = 0
    if (data.includes('  ')) {
      indexRegex = data.indexOf('  ')
    } else {
      indexRegex = data.indexOf('\n\n')
    }

    const content = data.slice(indexRegex + 1).replace(/\\\'/g,"\'")
    // let base64 = data.slice(0, indexRegex).replace(')', '').replace('(', '')
    // if (data.includes('dataimage')) {
    //   base64 = base64.slice(0, 4) + ':' + base64.slice(4, base64.length).replace('\\\\','\\')
    // }
    // console.log(base64)
    console.log(content)
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.logo} style={styles.logo} />
          </View>
          <View style={styles.section} >
            <Text style={styles.sectionText}>
              Lời giải
            </Text>
            <MathJax
              // HTML content with MathJax support
              html={content}
              // MathJax config option
              mathJaxOptions={{
                messageStyle: 'none',
                extensions: ['tex2jax.js'],
                jax: ['input/TeX', 'output/HTML-CSS'],
                tex2jax: {
                  inlineMath: [['$', '$'], ['\\(', '\\)']],
                  displayMath: [['$$', '$$'], ['\\[', '\\]']],
                  processEscapes: true,
                },
                TeX: {
                  extensions: ['AMSmath.js', 'AMSsymbols.js', 'noErrors.js', 'noUndefined.js','math.js']
                }
              }}

            />

            {/* <Image style={{width: Metrics.screenWidth-20, height: Metrics.screenHeight/3,alignSelf:'center'}} source={{uri: base64}}/>  */}
            {/* <Text style={styles.subtitle}>
            {content}
            </Text> */}

          </View>
        </ScrollView>
      </View>
    )
  }
}

export default connect(null, null)(AnswerScreen)