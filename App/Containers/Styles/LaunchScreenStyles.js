import { StyleSheet } from 'react-native'
import { Metrics, ApplicationStyles,Colors } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  container: {
    //paddingBottom: Metrics.baseMargin
  },
  logo: {
    marginTop: Metrics.doubleSection,
    height: Metrics.images.logo,
    width: Metrics.images.logo,
    resizeMode: 'contain',
  },
  centered: {
    alignItems: 'center'
  },
  cameraContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  cameraCapture: {
    flex: 0,
    backgroundColor: Colors.ember,
    borderRadius: 5,
    padding: 1,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20
  }
})
