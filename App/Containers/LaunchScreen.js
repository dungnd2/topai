import React, { Component } from 'react'
import { ScrollView, Text, Image, View, TouchableOpacity, ActivityIndicator } from 'react-native'
import { Images, Metrics } from '../Themes'
import { RNCamera } from 'react-native-camera';
import Modal from "react-native-modal";
import styles from './Styles/LaunchScreenStyles'
import { Icon } from 'react-native-elements'
import { connect } from 'react-redux';
import { ImageSelectors } from '../Redux/ImageRedux'
import ImageActions from '../Redux/ImageRedux'
import ImgToBase64 from 'react-native-image-base64';
import ImagePicker from 'react-native-image-picker';
import MathJax from 'react-native-mathjax'

const options = {
  title: 'Select Image',
  customButtons: [{ name: 'topica', title: 'Choose Photo' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
  quality: 1
};


class LaunchScreen extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props)
    this.state = {
      isVisible: false
    }
  }
  takePicture = async () => {
    try {
      const data = await this.camera.capture().then(() => {
        console.log('image saved')

      });
      console.log('Path to image: ' + data.path);

    } catch (err) {
      // console.log('err: ', err);
    }
  };


  uploadPic = () => {
    this.props.fetching()
    ImgToBase64.getBase64String(this.props.imageUrl)
      .then((base64String) => {
        console.log(base64String)
        this.props.convertBase64(base64String)
        this.props.uploadImages()
      })
      .catch(err => console.log(err));

  }

  openImagePicker = () => {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = response.uri;

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.props.saveImageUrl(source)
      }
    });
  }

  cropImage = () => {
    this.props.navigation.navigate('CameraScreen')
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <Image source={Images.background} style={styles.backgroundImage} resizeMode='stretch' />
        <ScrollView style={styles.container}>
          <View style={styles.centered}>
            <Image source={Images.logo} style={styles.logo} />
          </View>
          <View style={styles.section} >
            <TouchableOpacity style={styles.cameraCapture} onPress={this.openImagePicker}>
              <Text style={styles.sectionText}>
                CHỤP
            </Text>
            </TouchableOpacity>
            {this.props.imageUrl ?
              <TouchableOpacity onPress={this.cropImage}>

                <Image
                  style={{ justifyContent: 'center', alignContent: 'center', alignSelf: 'center', width: Metrics.screenWidth - 20, height: 300 }}
                  source={{ uri: this.props.imageUrl }}
                />
              </TouchableOpacity>
              :
              <Text style={styles.sectionText}>

              </Text>
            }
          </View>
        </ScrollView>
        <TouchableOpacity style={styles.cameraCapture} onPress={this.uploadPic}>
          {this.props.loading ?
            <ActivityIndicator style={{ justifyContent: 'center', alignContent: 'center', margin: 20 }} />
            :
            <Text style={styles.sectionText}>
              GIẢI
            </Text>
          }

        </TouchableOpacity>
      </View>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    imageUrl: ImageSelectors.selectImageUrl(state),
    loading: ImageSelectors.selectLoading(state)
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    uploadImages: () => dispatch(ImageActions.uploadImage()),
    convertBase64: (imageBase64) => dispatch(ImageActions.convertBase64(imageBase64)),
    saveImageUrl: (imageUrl) => dispatch(ImageActions.saveImage(imageUrl)),
    fetching: () => dispatch(ImageActions.fetching()),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(LaunchScreen)