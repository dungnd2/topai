import React, { Component } from 'react'
import { ScrollView, Text, Image, View, TouchableOpacity,StyleSheet } from 'react-native'
import { Images, Metrics } from '../Themes'
import { RNCamera } from 'react-native-camera';
import Modal from "react-native-modal";
import { Icon } from 'react-native-elements'
import { connect } from 'react-redux';
import ImageActions ,{ ImageSelectors }from '../Redux/ImageRedux'
import ImagePicker from 'react-native-image-crop-picker';

class TakePicture extends Component {
    static navigationOptions = {
        header:null
      };
  takePicture =  () => {
    ImagePicker.openCropper({
        path: this.props.imageUrl,
        width: Metrics.screenWidth,
        height: 200
      }).then(image => {
        console.log(image.path)
        this.props.saveImageUrl(image.path)
        this.props.navigation.navigate('LaunchScreen')
      }).catch(error => {
        this.props.navigation.navigate('LaunchScreen')
      })

    }
  
componentDidMount(){
    this.takePicture()
}
  render() {
    return (
      <View style={styles.container}>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      imageUrl: ImageSelectors.selectImageUrl(state)
    }
  }
  

const mapDispatchToProps = (dispatch) => {
    return {
        saveImageUrl: (imageUrl) => dispatch(ImageActions.saveImage(imageUrl)),
        convertBase64 :(imageBase64) => dispatch(ImageActions.convertBase64(imageBase64))
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(TakePicture)
const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: 'black'
    },
    preview: {
      flex: 1,
      justifyContent: 'flex-end',
      alignItems: 'center'
    },
    capture: {
      flex: 0,
      backgroundColor: 'transparent',
      borderRadius: 5,
      padding: 5,
      paddingHorizontal: 5,
      alignSelf: 'center',
      margin: 10
    }
  });