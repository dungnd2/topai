import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import styles from './ImageCropperStyles'
import { ScrollView } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { ImageSelectors } from '../../Redux/ImageRedux'

class ImageCropper extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }
    render() {
        return (
            <View style={styles.container} >
                <ScrollView style={styles.container} >
                    <Image
                        style={{ justifyContent: 'center', alignContent: 'center', alignSelf: 'center' }}
                        source={{ uri: this.props.imageUrl }}
                    />
                </ScrollView>
                <View style={styles.bottomContainer}>
                    <Text>Texxt</Text>
                    <Text>Texxt</Text>
                </View>
            </View>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        imageUrl: ImageSelectors.selectImageUrl(state),
    }
}
export default connect(mapStateToProps, null)(ImageCropper);