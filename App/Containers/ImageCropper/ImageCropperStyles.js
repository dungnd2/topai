import { StyleSheet } from 'react-native'
import Colors from '../../Themes/Colors';

export default styles = StyleSheet.create({
    container:{
        flex:1,
        //backgroundColor:Colors.black,
    },
    bottomContainer:{
        flexDirection:'row',
        marginBottom:0,
        backgroundColor:Colors.mainColor,
        justifyContent:'space-between'
    }
})