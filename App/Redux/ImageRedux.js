import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  saveImage: ['imageUrl'],
  uploadImage:['imageBase64'],
  convertBase64:['imageBase64'],
  uploadFinish:null,
  fetching:null
})

export const ImageTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
    imageUrl: '',
    loading:false,
    imageBase64:null
})

/* ------------- Selectors ------------- */

export const ImageSelectors = {
  selectImageUrl: state => state.image.imageUrl,
  selectLoading: state => state.image.loading
}

/* ------------- Reducers ------------- */

// request the avatar for a user
export const saveImage = (state, { imageUrl }) =>
  state.merge({ imageUrl})

// successful avatar lookup
export const uploadImage = (state) => {
  return state.merge()
}
export const fetching = (state) => {
  return state.merge({ loading: true})
}
// failed to get the avatar
export const convertBase64 = (state,{imageBase64}) =>{
    console.log(imageBase64)
    return state.merge({ imageBase64})
}
export const uploadFinish = (state) =>{
    return state.merge({ loading:false})
}

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SAVE_IMAGE]: saveImage,
  [Types.UPLOAD_IMAGE]: uploadImage,
  [Types.CONVERT_BASE64]: convertBase64,
  [Types.UPLOAD_FINISH]: uploadFinish,
  [Types.FETCHING]: fetching,
})
